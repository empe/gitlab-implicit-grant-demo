import { GitlabImplicitGrantDemoPage } from './app.po';

describe('gitlab-implicit-grant-demo App', () => {
  let page: GitlabImplicitGrantDemoPage;

  beforeEach(() => {
    page = new GitlabImplicitGrantDemoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
