import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {AuthGuard} from "./services/auth.guard";
import {UserResolve} from "./services/user-resolve.service";
import {UserService} from "./services/user.service";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '**',
    component: HomeComponent,
    canActivate: [ AuthGuard ],
    // resolve: {
    //   user: UserResolve
    // }
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  providers: [ UserResolve, UserService ]
})
export class RoutingModule { }
