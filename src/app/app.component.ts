import { Component } from '@angular/core';
import { OAuthService } from "angular-oauth2-oidc";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(private oAuthService: OAuthService) {
    this.oAuthService.tryLogin({
      onTokenReceived: context => {
        console.debug("logged in");
        console.debug(context);
      },
    })
  }

}
