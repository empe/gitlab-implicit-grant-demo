import { Component, OnInit } from '@angular/core';
import {User} from "../services/user";
import {OAuthService} from "angular-oauth2-oidc";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit{

  user: User;
  token: string;

  constructor(private oAuthService: OAuthService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(){
    this.user = this.route.snapshot.data['user'];
    this.token = this.oAuthService.getAccessToken();
    console.log(this.user);
  }


  logout():void {
    this.oAuthService.logOut(true);
    this.router.navigate(['/login']);
  }

}
