import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {OAuthModule} from "angular-oauth2-oidc";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {RoutingModule} from "./routing.module";
import {AuthGuard} from "./services/auth.guard";
import { TestComponent } from './login/test/test.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RoutingModule,
    OAuthModule.forRoot()
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
