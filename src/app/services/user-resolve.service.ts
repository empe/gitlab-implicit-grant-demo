import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {User} from "./user";
import {UserService} from "./user.service";

@Injectable()
export class UserResolve implements Resolve<User> {

  constructor(private service: UserService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.service.getUser()
  }

}
