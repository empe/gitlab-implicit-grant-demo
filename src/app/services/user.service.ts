import {Injectable} from '@angular/core';
import {OAuthService} from "angular-oauth2-oidc";
import {User} from "./user";

import {Observable} from "rxjs";
import {Headers, Http, Response} from "@angular/http";

@Injectable()
export class UserService {

  private endpoint = "http://localhost:3000/api/v4/user";

  constructor(private oauthService: OAuthService, private http:Http) {}

  public getUser(): Observable<User> {
    let headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.oauthService.getAccessToken());

    return this.http.get(this.endpoint, { headers: headers })
                    .map(this.extract)
  }

  private extract(res: Response){
    let body = res.json();
    return body;
  }


}
