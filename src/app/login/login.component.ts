import { Component } from '@angular/core';
import { OAuthService } from "angular-oauth2-oidc";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {



  constructor(private oAuthService: OAuthService) {}

  loginUrl = "http://localhost:3000/oauth/authorize";
  clientId = "<insert your Application Id here>";
  redirectUri = window.location.href.replace("login", "");

  login(): void {
    this.oAuthService.loginUrl = this.loginUrl;
    this.oAuthService.logoutUrl = this.redirectUri;
    this.oAuthService.clientId = this.clientId;
    this.oAuthService.redirectUri = this.redirectUri;
    this.oAuthService.oidc = false;
    this.oAuthService.scope = "api read_user";
    this.oAuthService.initImplicitFlow();
  }

}
