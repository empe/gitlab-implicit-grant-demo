# Gitlab Implicit Grant Demo
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.2.

### Running locally
Download and unpack the artifact from the `build` step. Unpack, enter the directory and run `python -m SimpleHTTPServer <port>`

### GitLab Pages
Go here -> https://empe.gitlab.io/gitlab-implicit-grant-demo/, adjust & run. 


